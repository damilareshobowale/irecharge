<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Home - Buy airtime online, buy MTN airtime, buy internet data subscription, Etisalat Glo Airtel Airtime, DSTV payment,GOTV payment,PHCN payment</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="active-preloader-ovh">




<?php include_once('inc_header.php'); ?>


<section class="banner-style-one" style="background-color: #FFFFFF;">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-6">
                <div class="banner-content">
                    <h3>So easy to recharge <br />with your phone, anytime.</h3>
                    <p>It is now so easy to recharge  your phone, cables and <br />utilities anytime, anywhere.</p>
                    <a href="register.php" class="banner-btn">Sign Up</a>  <a href="login.php" class="banner-btn">Login </a>
                </div><!-- /.banner-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <img src="img/banner-moc-1-1.png" alt="Awesome Image"/>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.thm-container -->
</section><!-- /.banner-style-one -->

<section class="features-style-one sec-pad">
    <div class="thm-container">
        <div class="sec-title text-center">
            <span>Suited For You</span>
            <h3>From various merchants and services</h3>
        </div><!-- /.sec-title text-center -->

        <div class="tab_row">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="active"><a data-toggle="tab" href="#tabAll" role="tab">All</a></li>
            <li><a data-toggle="tab" href="#airtime_recharge" role="tab">Airtime Recharge</a></li>
        </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#MTN-Airtime-Modal">
                        <img src="img/services/MTN-Airtime.jpg" alt="MTN-Airtime" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in" id="airtime_recharge">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#GLO-Airtime-Modal">
                        <img src="img/services/GLO-Airtime.jpg" alt="GLO-Airtime" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in" id="airtime_recharge">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#Airtel-Airtime-Modal">
                            <img src="img/services/Airtel-Airtime.jpg" alt="Airtel-Airtime" />
                        </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#9Mobile-Airtime-Modal">
                        <img src="img/services/9mobile-Airtime.jpg" alt="9Mobile-Airtime" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Pay-DSTV-Subscription.jpg" alt="Pay-DSTV-Susbcription" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#Airtel-Data-Modal">
                            <img src="img/services/Airtel-Data.jpg" alt="Airtel-Data" />
                        </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#MTN-Data-Modal">
                        <img src="img/services/MTN-Data.jpg" alt="MTN-Data" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#GLO-Data-Modal">
                        <img src="img/services/GLO-Data.jpg" alt="MTN-Data" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#9Mobile-Data-Modal">
                            <img src="img/services/9mobile-Data.jpg" alt="9mobile-Data" />
                        </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Gotv-Payment.jpg" alt="GoTV-Payment" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/MTN-Blackberry-Data.jpg" alt="MTN-Blackberry-Data" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                        <a href="">
                            <img src="img/services/Startimes-Subscription.jpg" alt="Startimes-Subscription" />
                        </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Ikeja-Electric-Payment-PHCN.jpg" alt="Ikeja-Electric-Payment-PHCN" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Eko-Electric-Payment-PHCN.jpg" alt="Eko-Electric-Payment-PHCN" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Jos-Electric-JED.jpg" alt="Jos-Electric-JED" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Port-Harcourt-Electric.jpg" alt="Port-Harcourt-Electric" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Kano-Electric.jpg" alt="Kano-Electric" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Abuja-Electric.jpg" alt="Abuja-Electric" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Website-Design-Deal-Hostparker.jpg" alt="Website-Design-Deal-Hostparker" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Bank-Deposit.jpg" alt="Bank-Deposit" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/SMSclone.com.jpg" alt="SMSclone.com" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/9Mobile-Airtime-Pin.jpg" alt="9Mobile-Airtime-Pin" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/MTN-Airtime-Pin.jpg" alt="MTN-Airtime-Pin" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Glo-Airtime-Pin.jpg" alt="GLO-Airtime-Pin" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/Airtel-Airtime-Pin.jpg" alt="Airtel-Airtime-Pin" />
                    </a>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-center home-services tab-pane fade in">
                    <a href="">
                        <img src="img/services/WAEC-Registration-PIN.jpg" alt="WAEC-Registration-Pin" />
                    </a>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div>
    </div><!-- /.thm-container -->
</section><!-- /.features-style-one -->

<section class="video-box-style-one">
    <div class="thm-container">
        <div class="sec-title text-center">
            <span>Short Clip</span>
            <h3>So easy how it works</h3>
        </div><!-- /.sec-title text-center -->
        <div class="video-box-content">
            <div class="img-box">
                <img src="img/video-1-1.jpg" alt="Awesome Image"/>
                <a href="//www.youtube.com/watch?v=5F3SP8dcyjo" class="video-popup "><span class="inner hvr-pulse"><i class="fa fa-play"></i></span></a>
            </div><!-- /.img-box -->
        </div><!-- /.video-box-content -->
    </div><!-- /.thm-container -->
</section><!-- /.video-box-style-one -->




<?php include_once('inc_footer.php'); ?>


<div class="scroll-to-top scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></div>                    

<script src="js/jquery.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).on('load',function(){
    $('#tabAll').parent().addClass('active');  
      $('.tab-pane').addClass('active in');  
    $('[data-toggle="tab"]').parent().removeClass('active');
  });
</script>

</body>
</html>

<!-- Different Modals -->

<div class="modal fade" id="MTN-Airtime-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #FBCC0C;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">MTN Airtime VTU</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>MTN airtime - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="rphonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="mtn-airtime" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/MTN-Airtime.jpg" alt="MTN-Airtime" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #fbcc0c;border-color: #fbcc0c;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="GLO-Airtime-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #4AB940;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">GLO Airtime VTU</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>GLO airtime - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="rphonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="glo-airtime" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/GLO-Airtime.jpg" alt="GLO-Airtime" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #4AB940;border-color: #4AB940;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="Airtel-Airtime-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ED1B24;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">Airtel Airtime VTU</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>Airtel airtime - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="rphonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="airtel-airtime" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/Airtel-Airtime.jpg" alt="Airtel-Airtime" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #ED1B24;border-color: #ED1B24;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="9Mobile-Airtime-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #006d52;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">9Mobile Airtime VTU</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>9Mobile airtime - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="airtime_recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="phonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="9mobile-airtime" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/9mobile-Airtime.jpg" alt="9Mobile-Airtime" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #006d52;border-color: #006d52;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="MTN-Data-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #FBCC0C;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">MTN Data</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>MTN Data - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                    <label for="data_value" class="col-form-label">Data Value:</label>
                                    <select class="form-control" id="data_value" name="data_value" onchange="updateDataAmount(this.value);">
                                        <option value="">Please select a data value </option>
                                        <option value="100.00">&#8358;100 30MB 24 hours </option>
                                        <option value="200.00">&#8358;200 100MB 24 hours </option>
                                        <option value="500.00">&#8358;500 750MB 7 days </option>
                                        <option value="1000.00">&#8358;1,000 1.5GB 30 days </option>
                                        <option value="2000.00">&#8358;2,000 3.5GB 30 days</option>
                                        <option value="5000.00">&#8358;5,000 MTN Data Bundle</option>
                                    </select>
                            </div> 
                            <hr>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="phonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="integer" class="form-control" id="amount" name="amount" readonly>
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="mtn-data" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/MTN-Data.jpg" alt="MTN-Data" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #FBCC0C;border-color: #FBCC0C;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="GLO-Data-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #4AB940;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">GLO Data</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>GLO Data - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                    <label for="data_value" class="col-form-label">Data Value:</label>
                                    <select class="form-control" id="data_value" name="data_value" onchange="updateDataAmount(this.value);">
                                        <option value="">Please select a data value </option>
                                        <option value="100.00">&#8358;100 30MB 24 hours </option>
                                        <option value="200.00">&#8358;200 100MB 24 hours </option>
                                        <option value="500.00">&#8358;500 750MB 7 days </option>
                                        <option value="1000.00">&#8358;1,000 1.5GB 30 days </option>
                                        <option value="2000.00">&#8358;2,000 3.5GB 30 days</option>
                                        <option value="5000.00">&#8358;5,000 MTN Data Bundle</option>
                                    </select>
                            </div> 
                            <hr>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="phonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="integer" class="form-control" id="amount" name="amount" readonly>
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="mtn-data" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/GLO-Data.jpg" alt="GLO-Data" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #4AB940;border-color: #4AB940;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="Airtel-Data-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ED1B24;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">Airtel Data</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>Airtel Data - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                    <label for="data_value" class="col-form-label">Data Value:</label>
                                    <select class="form-control" id="data_value" name="data_value" onchange="updateDataAmount(this.value);">
                                        <option value="">Please select a data value </option>
                                        <option value="100.00">&#8358;100 30MB 24 hours </option>
                                        <option value="200.00">&#8358;200 100MB 24 hours </option>
                                        <option value="500.00">&#8358;500 750MB 7 days </option>
                                        <option value="1000.00">&#8358;1,000 1.5GB 30 days </option>
                                        <option value="2000.00">&#8358;2,000 3.5GB 30 days</option>
                                        <option value="5000.00">&#8358;5,000 MTN Data Bundle</option>
                                    </select>
                            </div> 
                            <hr>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="phonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="integer" class="form-control" id="amount" name="amount" readonly>
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="mtn-data" />
                    </form>
                </div>
                <div class="col-md-6 hidden-xs">
                    <img src="img/services/Airtel-Data.jpg" alt="Airtel-Data" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #ED1B24;border-color: #ED1B24;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>

<div class="modal fade" id="9Mobile-Data-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" href="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #006d52;">
        <h2 class="modal-title" id="exampleModalLabel" style="color: #FFFFFF">9Mobile Data</h2>
      </div>
      <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                    <p>9Mobile Data - Get instant Top up </p>
                    <hr />
                    <form action="admin/actionmanager.php" id="recharge_form" role="form" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                    <label for="data_value" class="col-form-label">Data Value:</label>
                                    <select class="form-control" id="data_value" name="data_value" onchange="updateDataAmount(this.value);">
                                        <option value="">Please select a data value </option>
                                        <option value="100.00">&#8358;100 30MB 24 hours </option>
                                        <option value="200.00">&#8358;200 100MB 24 hours </option>
                                        <option value="500.00">&#8358;500 750MB 7 days </option>
                                        <option value="1000.00">&#8358;1,000 1.5GB 30 days </option>
                                        <option value="2000.00">&#8358;2,000 3.5GB 30 days</option>
                                        <option value="5000.00">&#8358;5,000 MTN Data Bundle</option>
                                    </select>
                            </div> 
                            <hr>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="phonenumber" class="col-form-label">Phone Number:</label>
                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="amount" class="col-form-label">Amount:</label>
                            <input type="integer" class="form-control" id="amount" name="amount" readonly>
                        </div>
                        <div class="form-group">
                            <label for="discountvoucher" class="col-form-label">Discount Voucher:</label>
                            <input type="text" class="form-control" id="discountvoucher" name="discountvoucher">
                        </div>
                            </div>
                        </div>
                        <input type="hidden" id="recharge_type" name="recharge_type" value="9mobile-data" />
                    </form>
                </div>
                <div class="col-md-6">
                    <img src="img/services/9Mobile-Data.jpg" alt="9Mobile-Data" />
                </div>
           </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" style="background-color: #006d52;border-color: #006d52;">Continue</button>
      </div>
    </div>
  </div>
 </div>
</div>