<footer class="footer-style-one">    
        <div class="footer-top clearfix">
            <div class="thm-container">
                <a href="index.html" class="footer-logo pull-left"><img src="img/footer-logo-1.png" alt="Awesome Image"/></a>
                <div class="social pull-right">
                    <a href="#" class="fa fa-twitter"></a><!--
                    --><a href="#" class="fa fa-facebook"></a><!--
                    --><a href="#" class="fa fa-instagram"></a><!--
                    --><a href="#" class="fa fa-google-plus"></a>
                </div><!-- /.social pull-right -->
            </div><!-- /.thm-container -->
        </div><!-- /.footer-top -->
        <div class="footer-bottom text-center">
            <div class="thm-container">
                <p>&copy; <?php echo date('Y'); ?> Copyrights. <a href="#">iTecharge.com</a></p>
            </div><!-- /.thm-container -->
        </div><!-- /.footer-bottom text-center -->
</footer><!-- /.footer-style-one -->