<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Login - MBtop up</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="active-preloader-ovh">





<?php include_once('inc_header.php'); ?>


<div class="inner-banner">
    <div class="thm-container clearfix">
        <div class="title text-center">
            <h3>Contact Us</h3>
        </div><!-- /.title pull-left -->
    </div><!-- /.thm-container clearfix -->
</div><!-- /.inner-banner -->

<section class="contact-page-content gray-bg sec-pad">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-7">
                <form action="inc/sendemail.php" class="contact-form">
                    <input type="text" placeholder="Your name" name="name" />
                    <input type="text" placeholder="Email address"  name="email" />
                    <textarea name="message" placeholder="Write message here"></textarea>
                    <button type="submit" class="hvr-sweep-to-right">Send Message</button>
                </form><!-- /.contact-form -->
            </div><!-- /.col-md-7 -->
            <div class="col-md-5">
                <div class="contact-info">
                    <span>Contact us</span>
                    <h3>Do you have any question? don’t hesitate to contact with us </h3>
                    <div class="single-contact-info">
                        <h4>Call us for imiditate support on this number</h4>
                        <p>(+234) 813 636 8738</p>
                    </div><!-- /.single-contact-info -->
                    <div class="single-contact-info">
                        <h4>Send us email for any kind of inquiry</h4>
                        <p>info@mxtopup.com</p>
                    </div><!-- /.single-contact-info -->
                    <div class="single-contact-info">
                    <h4>Or via social platform</h4>
                    <div class="social">
                    <a href="#" class="fa fa-twitter"></a><!--
                    --><a href="#" class="fa fa-facebook"></a><!--
                    --><a href="#" class="fa fa-instagram"></a><!--
                    --><a href="#" class="fa fa-google-plus"></a>
                </div><!-- /.social pull-right -->
                    </div>
                </div><!-- /.contact-info -->
            </div><!-- /.col-md-5 -->
        </div><!-- /.row -->
    </div><!-- /.thm-container -->
</section><!-- /.contact-page-content -->

<div 
  class="google-map" 
  id="contact-google-map" 
  data-map-lat="44.712784" 
  data-map-lng="-72.005941" 
  data-icon-path="img/map-marker.png"
  data-map-title="Brooklyn, New York, United Kingdom"
  data-map-zoom="13"
  data-markers='{
            "marker-1": [44.712784, -72.005941, "<h4>Main Office</h4><p>Babylon Branch , Lindenhurst, UK</p>"]
        }'>

 </div>


<?php include_once('inc_footer.php'); ?>


<div class="scroll-to-top scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></div>                    

<script src="js/jquery.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>