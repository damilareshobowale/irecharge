<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Login - MBtop up</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="active-preloader-ovh">





<?php include_once('inc_header.php'); ?>


<div class="inner-banner">
    <div class="thm-container clearfix">
        <div class="title text-center">
            <h3>LOGIN</h3>
        </div><!-- /.title pull-left -->
    </div><!-- /.thm-container clearfix -->
</div><!-- /.inner-banner -->

<section class="contact-page-content sec-pad">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-7 text-center">
                <form action="inc/sendemail.php" class="contact-form" >
                    <input type="text" placeholder="Your email or username" name="username" />
                    <input type="password" placeholder="Enter password"  name="password" />
                    <button type="submit" class="hvr-sweep-to-right">Login </button>
                </form><!-- /.contact-form -->
                <div class="subtext">
                    <p><a href="register.php" class="pull-left" >New account? </a> <a href="forget-password.php" class="pull-right" >Forgot password? </a></p>
                </div>
            </div><!-- /.col-md-7 -->
            <div class="col-md-5 hidden-xs">
                <img src="img/blog-1-1.jpg" alt="Awesome Image"/>
            </div>
        </div><!-- /.row -->
    </div><!-- /.thm-container -->
</section><!-- /.contact-page-content -->


<?php include_once('inc_footer.php'); ?>


<div class="scroll-to-top scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></div>                    

<script src="js/jquery.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>