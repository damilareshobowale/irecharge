/** customized javascripts functions */

/** 1. Toggle password check and view */
function checkPassword()
{
    var typeinput = $("input[id='password']").attr('type');
    if (typeinput == 'password')
    {
        $("input[id='password']").attr('type', 'text');
        $("input[id='password2']").attr('type', 'text');
        $('.feather-icon').html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>')
    }
    else {
        $("input[id='password']").attr('type', 'password');
        $("input[id='password2']").attr('type', 'password');
        $('.feather-icon svg').html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>');
    }
    return;
}


/** 2. Alert Notiications */
function errorAlert(message)
{
    $('#result').html('<div class="alert alert-inv alert-inv-danger alert-wth-icon alert-dismissible fade show" role="alert">'+ 
                        '<span class="alert-icon-wrap"><i class="fa fa-warning"></i></span> '+ message + 
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>' +
                    '</div>');
    return false;
}

function successAlert(message)
{
    $('#result').html('<div class="alert alert-inv alert-inv-success alert-wth-icon alert-dismissible fade show" role="alert">'+ 
                        '<span class="alert-icon-wrap"><i class="zmdi zmdi-bug"></i></span> '+ message + 
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>' +
                    '</div>');
    return false;
}


/** Submissions of Forms Through AJAX */
/** 1. Register form */
$('#registerform').on('submit', function(event){
    
    
    event.preventDefault();

    /* get some values from elements on the page: */
    var $form = $(this),
            referral = $form.find('input[name="referral"]').val(),
            username = $form.find('input[name="username"]').val(),
            email = $form.find('input[name="email"]').val(),
            password = $form.find('input[name="password"]').val(),
            confirmpassword = $form.find('input[name="confirmpassword"]').val(),
            url = $form.attr('action');


    /** Check if the password and confirm password is the same */
    $msg = '';

    if (username.length < 3)
    {
        $msg += 'Username must be more than 2<br />'
    }

    if (password.length < 6)
    {
        $msg += 'Password must be more than 5<br />'
    }


    if (confirmpassword !== password) 
    {
        $msg += 'Password do not match <br />';
    }


    if ($msg != '')
    {
        errorAlert($msg);
        return;
    }

    /** AJAX backend validations */
    /* Send the data using post */
     var posting = $.post(url, {
        referral: referral,
        username: username,
        email: email,
        password: password,
        command: "members_add" 
    });
    
    /* Put the results in a div */
    posting.done(function(data) {
        $("#result").html(data);              
    });

    return;
});

/** 1. Login form */
$('#loginform').on('submit', function(event){
    
    
    event.preventDefault();

    /* get some values from elements on the page: */
    var $form = $(this),
            username = $form.find('input[name="username"]').val(),
            password = $form.find('input[name="password"]').val(),
            url = $form.attr('action');
    

    /** AJAX backend validations */
    /* Send the data using post */
     var posting = $.post(url, {
        username: username,
        password: password,
        command: "members_login" 
    });
    
    /* Put the results in a div */
    posting.done(function(data) {
        $("#result").html(data);              
    });

    return;
});