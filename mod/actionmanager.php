<?php
require_once("config.php");
require_once("inc_dbfunctions.php");

$actionmanager = New ActionManager();

if(isset($_POST['command']) && $_POST['command'] == 'members_add')
{
    $actionmanager->members_add();
}
else if(isset($_POST['command']) && $_POST['command'] == 'members_login')
{
    $actionmanager->members_login();
}

class ActionManager
{
    function members_add()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $referral = $_POST['referral'];
        
        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        $count = 0;
        $msg = '';
        
        
        //check if username exists
        $username_check = $dataRead->member_getbyusername($mycon, $username);
        if ($username_check != false)
        {
            $msg = "Username has already been used! <br />";
            $count += 1;
        }

        //check if email exists
        $email_check = $dataRead->member_getbyemail($mycon,$email);
        if ($email_check != false)
        {
            $msg = 'Email has already been used! <br />';           
            $count += 1;
        }
        
        if ($referral == $username || $referral == $email)
        {
            $msg = 'You cannot use yourself as a referral <br />';
            $count += 1;
        }
        //get the member_id of the referral
        if ($referral != null)
        {
          $referral_id = $dataRead->member_referral($mycon, $referral);
            if (!$referral_id)
            {
                $msg = 'Sorry, your referral ID does not exists. <br />';
                $count += 1;
            }  
        }
        else {
            $referral_id['member_id'] = '1';
        }
        

         if ($count != 0)
        {
            echo "<script type='text/javascript'>
                        errorAlert(".$msg.");
                    </script>";
            return;
        }

        //encode the password
        $password = generatePassword($password);
        
        //if all was successful, send a message to the email of the person so as to continue its registrations
        $member_id = $dataWrite->members_add($mycon,$username,$password,$email,$referral_id['member_id']);

        if (!$member_id)
        {
            echo "<script type='text/javascript'>
                    errorAlert('There was an error processing your request, please try again later');
                    $('input').html('');
                </script>";;
            return;
        }
        else 
        {
            /** Registration was successful, please proceed to login for the first time */
            echo "<script type-'text/javascript'>
            successAlert('Your account successfully created, please proceed to login for the first time');
                window.setTimeout(function(){
            document.location.href='login.php';
            },2000);
            </script>";
            return;

        }

    
    }

    function members_login()
    {
        $mycon = databaseConnect();
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        $dataread = New DataRead();
        $dataWrite = New DataWrite();
        
        //generate the encoded password
        $password = generatePassword($password);
        $msg = '';
    
        
        //check whether the email and password exists
        $member_get = $dataread->member_getbyusernamepassword($mycon, $username, $password);
        

        if(!$member_get)
        {
            $msg .= 'Username or password is wrong <br />';
        }

        if ($member_get['status'] == '0')
        {
            $msg .= 'Your account has been disabled, please kindly contact us <br />';
        }
        
        if ($msg !== '')
        {
            echo "<script type='text/javascript'>
                        errorAlert('".$msg."');
                    </script>";
            return;
        }
        
        
        createCookie("userid",$member_get['member_id']);
        createCookie("userlogin","YES");
        createCookie("adminlogin", "NO");
        createCookie("username",$member_get['username']);
        createCookie("email", $member_get['email']);
        
         echo "<script type-'text/javascript'>
                successAlert('Login success, redirecting!');
                    window.setTimeout(function(){
                document.location.href='dashboard.php';
            },2000);
                </script>";
        return;
    }
 


}
?>