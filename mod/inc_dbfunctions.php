<?php



class DataWrite
{   
   

    //create the useracccount
    function members_add($mycon,$username,$password,$email,$referral_id)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `members` SET `username` = :username
          ,`email` = :email
          ,`password` = :password
          ,`referral_id` = :referral_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":username", $username, PDO::PARAM_STR);
      $myrec->bindValue(":password", $password,PDO::PARAM_STR);
      $myrec->bindValue(":email", $email,PDO::PARAM_STR);
      $myrec->bindValue(":referral_id", $referral_id,PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;
      
      return $mycon->lastInsertId();
      
    }
}


class DataRead {

    //check if username exists
    function member_getbyusername($mycon,$username)
    {
        $sql = "SELECT * FROM `members` WHERE `username` = :username LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $username);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the details of the referral
    function member_referral ($mycon,$referral)
    {
        $sql = "SELECT * FROM `members` WHERE `username` = :username OR `email` = :email LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $referral);
        $myrec->bindValue(":email", $referral);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the member_id of the referral
    function member_getbyusernamepassword($mycon,$username, $password)
    {
        $sql = "SELECT * FROM `members` WHERE (`username` = :username OR `email` = :email) AND `password` = :password LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $username);
        $myrec->bindValue(":email", $username);
        $myrec->bindValue(":password", $password);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the user by email
    function member_getbyemail($mycon,$email)
    {
        $sql = "SELECT * FROM `members` WHERE `email` = :email LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":email", $email);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }





}


?>