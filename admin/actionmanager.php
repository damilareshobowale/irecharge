<?php
require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
require 'config.php';
$actionmanager = New ActionManager();
if (isset($_POST['command']) && $_POST['command'] == 'contact_us')
{
	$actionmanager->contact_us();
}
elseif(isset($_POST['command']) && $_POST['command'] == 'join_us_add')
{
	$actionmanager->join_us_add();
}

class ActionManager
{
	function contact_us()
	{
		$name = $_POST['fullName'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$txt = $_POST['message'];

		$error = 0;
		$msg = '';
		if ($name == '')
		{
			$msg .= 'Your name is required \n';
			$error += 1;
		}

		if ($email == '')
		{
			$msg .= 'Your email is required \n';
			$error += 1;
		}

		if ($txt == '')
		{
			$msg .= 'Please type in your message \n';
			$error += 1;
		}

		if ($error > 0)
		{
			showAlert($msg);
			return;
		}

		//send the message
		$message = "<div>
						<p> Hello admin, ".$name."sent a message was sent from the contact form of the website, please find details here: </p><br>
						<p>Name: ". $name."</p>
						<p>Email: ".$email."</p>
						<p>Phone Number: ".$phonenumber."</p>
						<p>Message: ".$txt."</p>
						<br><br>
						Thank you Admin.
					</div>";
		$replymessage = "<div>
							<p> Hello ".$name." Your message has been received. We will definitely get back to you as soon as possible. If by any means we didn't get back to you, please ensure you call us through <strong> +49(0) 228 24060340 or info@afridat.org.</strong> </p>
							<br><br>
							Thank You for contacting us. <br>
							AFRIDAT.ORG <br />
							<a href='http://afridat.org' target='_blank'>http://afridat.org</a>
							</div>";

		if(sendEmail($email, 'Thank You For Contacting Us', $replymessage) && sendEmail('info@afridat.org', 'Message from website', $message))
		{
			echo "<script type='text/javascript'>
					$('.form-control').val('');
				</script>";
			showAlert('Your message has been delivered. Please check your inbox to confirm');
			return;
		}
		else
		{
			showAlert('Your message could not be sent. Please try again later.');
			return;
		}

		return;

	}

	function join_us_add()
	{
		$fullname = $_POST['full_name'];
		$email = $_POST['email'];
		$mobile_number = $_POST['mobile_number'];
		$home_address = $_POST['home_address'];
		$country_of_residence = $_POST['country_of_residence'];
		$city_of_residence = $_POST['city_of_residence'];
		$country_of_origin = $_POST['country_of_origin'];
		$gender = $_POST['gender'];
		$short_motivation = $_POST['short_motivation'];
		$profession = $_POST['profession'];
		$title = $_POST['title'];
		$area_of_specialisation = $_POST['area_of_specialisation'];
		$agreement = $_POST['agreement'];

		$error = 0;
		$msg = '';
		if ($title == '')
		{
			$msg .= 'Title is required \n';
			$error += 1;
		}

		if ($fullname == '')
		{
			$msg .= 'Your full name is required \n';
			$error += 1;
		}

		if ($email == '')
		{
			$msg .= 'Your email is required \n';
			$error += 1;
		}

		if ($mobile_number == '')
		{
			$msg .= 'Please mobile number is required \n';
			$error += 1;
		}
		if ($home_address == '')
		{
			$msg .= 'Please fill in your home address \n';
			$error += 1;
		}

		if ($country_of_residence == '')
		{
			$msg .= 'Your country of residence is required \n';
			$error += 1;
		}

		if ($country_of_origin == '')
		{
			$msg .= 'Country of origin is required \n';
			$error += 1;
		}

		if ($gender == '')
		{
			$msg .= 'No gender selected \n';
			$error += 1;
		}

		if ($profession == '')
		{
			$msg .= 'Your profession is required \n';
			$error += 1;
		}

		if (strlen($short_motivation) > 250 || strlen($short_motivation) < 3)
		{
			$msg .= 'Short motivation is invalid \n';
			$error += 1;
		}

		if (strlen($area_of_specialisation) < 2)
		{
			$msg .= 'Please enter at least one area of Specialisation \n';
			$error += 1;
		}

		if (!isset($agreement))
		{
			$msg .= 'Please agree to our terms and condition for the application  \n';
			$error += 1;
		}

		if ($error > 0)
		{
			showAlert($msg);
			return;
		}

		//send the message
		$message = "<div>
						<p> Hello admin, ".$fullname." is interested in joining the team of expert in the company, please kindly find more information in this mail: </p><br>
						<p>Title: ". $title."</p>
						<p>Full Name: ". $fullname."</p>
						<p>Email: ".$email."</p>
						<p>Mobile Number: ".$mobile_number."</p>
						<p>Home Address: ".$home_address."</p>
						<p>Country of Residence: ". $country_of_residence."</p>
						<p>City of Residence: ".$city_of_residence."</p>
						<p>Country of Origin: ".$country_of_origin."</p>
						<p>Gender: ". $gender."</p>
						<p>Profession: ".$profession."</p>
						<p>Area of Specialisation: ".$area_of_specialisation."</p>
						<p>Short Motivation: ".$short_motivation."</p>
						<br><br>
						Thank you Admin.
					</div>";
		$replymessage = "<div>
							<p> Hello ".$title. " ".$fullname.", Your application has been received. We will definitely get back to you for further information as soon as possible. </p>
							<br><br>
							Thank You for Sending Your Application. <br>
							AFRIDAT.ORG <br />
							<a href='http://afridat.org' target='_blank'>http://afridat.org</a>
							</div>";

		if(sendEmail($email, 'Thank You For Applying', $replymessage) && sendEmail('info@afridat.org', 'Message from website', $message))
		{
			echo "<script type='text/javascript'>
					$('.form-control').val('');
				</script>";
			showAlert('Your application was delivered. Please check your inbox to confirm');
			openPage('/join_us');
		}
		else
		{
			showAlert('Your message could not be sent. Please try again later.');
			return;
		}

		return;

	}
}


?>