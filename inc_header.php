<div class="preloader"><div class="spinner"></div></div> <!-- /.preloader -->


<header class="header header-home-one">
    <nav class="navbar navbar-default header-navigation">
        <div class="thm-container clearfix">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed mixup-icon-menu" data-toggle="collapse" data-target=".main-navigation" aria-expanded="false"> </button>
                <a class="navbar-brand" href="index.php">
                    <img src="img/header-logo-1.png" alt="Awesome Image"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse main-navigation mainmenu " id="main-nav-bar">
                
                <ul class="nav navbar-nav navigation-box">
                    <li> 
                        <a href="payment.php">Make Payment</a> 
                    </li>
                    <li> <a href="mod/register.php">Register</a> </li>
                    <li> 
                        <a href="mod/login.php">Login</a> 
                    </li>
                    <li> <a href="about.php">About us</a> </li>
                    <li> <a href="contact-us.php">Contact us</a> </li>
                </ul>                
            </div><!-- /.navbar-collapse -->
            <div class="right-side-box">
                <div class="social">
                    <a href="#" class="fa fa-twitter"></a><!--
                    --><a href="#" class="fa fa-facebook"></a><!--
                    --><a href="#" class="fa fa-instagram"></a>
                </div><!-- /.social -->
            </div><!-- /.right-side-box -->
        </div><!-- /.container -->
    </nav>   
</header><!-- /.header -->